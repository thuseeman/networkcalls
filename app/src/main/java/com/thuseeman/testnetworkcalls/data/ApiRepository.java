package com.thuseeman.testnetworkcalls.data;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiRepository {

    private static ApiService apiService;

    public static ApiService getApiService(){
        if (apiService == null){
            apiService = new Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("https://reqres.in/api/")
                    .build()
                    .create(ApiService.class);
        }
        return apiService;
    }
}
