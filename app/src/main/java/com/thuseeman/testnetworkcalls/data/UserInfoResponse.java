package com.thuseeman.testnetworkcalls.data;

import com.google.gson.annotations.SerializedName;
import com.thuseeman.testnetworkcalls.model.UserInfo;

public class UserInfoResponse {

    @SerializedName("data")
    UserInfo userInfo;

    public UserInfo getUserInfo() {
        return userInfo;
    }
}
