package com.thuseeman.testnetworkcalls.data;

import com.google.gson.annotations.SerializedName;
import com.thuseeman.testnetworkcalls.model.UserId;

import java.util.List;

public class UserIdResponse {

    @SerializedName("data")
    List<UserId> userIdList;

    public List<UserId> getUserIdList() {
        return userIdList;
    }
}
