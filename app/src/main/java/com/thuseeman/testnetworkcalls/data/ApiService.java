package com.thuseeman.testnetworkcalls.data;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiService {

    @GET("users?page=2")
    Flowable<UserIdResponse> fetchUserIdList();

    @GET("users/{id}")
    Flowable<UserInfoResponse> fetchUserInfo(@Path("id")int userId);
}
