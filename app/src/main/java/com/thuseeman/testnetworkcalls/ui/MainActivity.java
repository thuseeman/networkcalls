package com.thuseeman.testnetworkcalls.ui;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;

import com.thuseeman.testnetworkcalls.R;
import com.thuseeman.testnetworkcalls.data.ApiRepository;
import com.thuseeman.testnetworkcalls.data.ApiService;
import com.thuseeman.testnetworkcalls.databinding.ActivityMainBinding;
import com.thuseeman.testnetworkcalls.model.UserInfo;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    private ApiService apiService = ApiRepository.getApiService();
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private List<UserInfo> userInfoList = new ArrayList<>();
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        UserInfoAdapter adapter = new UserInfoAdapter(userInfoList);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setAdapter(adapter);
        fetchData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();
    }

    private void fetchData(){
        binding.progressBar.setVisibility(View.VISIBLE);
        compositeDisposable.add(
                apiService.fetchUserIdList()
                        .subscribeOn(Schedulers.io())
                        .flatMapIterable(userIdResponse -> userIdResponse.getUserIdList())
                        .flatMap(userId -> apiService.fetchUserInfo(userId.getId()))
                        .map(userInfoResponse -> userInfoResponse.getUserInfo())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(userInfo -> {
                            userInfoList.add(userInfo);
                            binding.recyclerView.getAdapter().notifyDataSetChanged();
                            Log.d(TAG, userInfo.toString());
                            binding.progressBar.setVisibility(View.GONE);
                        }, e -> {
                            Log.d("Error : ", e + "");
                            e.printStackTrace();
                            binding.progressBar.setVisibility(View.GONE);
                        })

        );
    }
}
