package com.thuseeman.testnetworkcalls.ui;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;
import com.thuseeman.testnetworkcalls.R;
import com.thuseeman.testnetworkcalls.databinding.UserInfoItemBinding;
import com.thuseeman.testnetworkcalls.model.UserInfo;

import java.util.List;

public class UserInfoAdapter extends RecyclerView.Adapter<UserInfoAdapter.ViewhHolder> {
    private List<UserInfo> userInfoList;

    public UserInfoAdapter(List<UserInfo> userInfoList) {
        this.userInfoList = userInfoList;
    }

    @NonNull
    @Override
    public ViewhHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        UserInfoItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                R.layout.user_info_item, viewGroup, false);
        return new ViewhHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewhHolder viewhHolder, int i) {
        UserInfo userInfo = userInfoList.get(i);
        viewhHolder.binding.userId.setText(userInfo.getId()+"");
        viewhHolder.binding.name.setText(userInfo.getFullName());
        Picasso.get()
                .load(userInfo.getAvatar())
                .into(viewhHolder.binding.image);
    }

    @Override
    public int getItemCount() {
        return userInfoList.size();
    }

    public class ViewhHolder extends RecyclerView.ViewHolder {
        UserInfoItemBinding binding;
        public ViewhHolder(@NonNull UserInfoItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
