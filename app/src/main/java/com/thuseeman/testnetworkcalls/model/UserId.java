package com.thuseeman.testnetworkcalls.model;

import java.util.ArrayList;
import java.util.List;

public class UserId {

    int id;

    public UserId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static List<UserId> getUserIdList(){
        List<UserId> list = new ArrayList<>();
        list.add(new UserId(442));
        list.add(new UserId(443));
        list.add(new UserId(444));
        return list;
    }

    @Override
    public String toString() {
        return "UserId{" +
                "id=" + id +
                '}';
    }
}
